.PHONY: build clean install install-modules clean-modules run


LUA_VERSION = 5.1
LUA_TREE    = lua_modules

LUA         = luajit
LUAROCKS    = luarocks --lua-version=$(LUA_VERSION) --tree $(LUA_TREE)
MOONC       = $(LUA_TREE)/bin/moonc

SRC         = $(shell find . -name "*.moon")
OUT         = $(SRC:.moon=.lua)


build: $(OUT)

%.lua: %.moon
	@$(MOONC) $^

clean:
	$(RM) $(OUT)

install:


install-modules: clean-modules
	$(LUAROCKS) install moonscript
	# Add your dependencies here

	# mkdir lib

	# TIMOOP
	# git clone https://gitlab.com/Thilawyn/timoop.git /tmp/timoop
	# cp -r /tmp/timoop/timoop lib

	# Add your git dependencies here (only recommended for development)

clean-modules:
	$(RM) -r $(LUA_TREE)
	# $(RM) -r lib


run: build
	$(LUA) -l paths main.lua
